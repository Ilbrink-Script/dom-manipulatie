/**
 * Part 1
 */

const name = "Paul";

// handle button click
const buttonClick = () => {
    // construct greeting
    const greeting = `Hi ${name}!`;
        
    // inject h1 with greeting
    const heading = document.getElementById("heading");
    heading.innerText = greeting;

    // change the appearance (for Part 2)
    changeColor(heading);
}

/**
 * Part 2
 */
const colors = ['red', 'orange', 'yellow', 'green', 'blue', 'purple'];

// styles the element with a random picked color
const changeColor = element => {
    const pickedColor = colors[Math.floor(Math.random() * colors.length)];
    element.setAttribute("style", `color: ${pickedColor}`);
};